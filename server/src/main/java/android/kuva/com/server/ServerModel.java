package android.kuva.com.server;

import android.util.Log;

import com.android.kuva.mymodule.City;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by KuVa on 13.08.2015.
 */
public class ServerModel
{
    public final static String TAG = "ServerModel";

    public final static String URL = "https://api.parse.com/1/classes/City";
    public final static String WHERE_VALUE = "{\"User\":{\"__type\":\"Pointer\",\"className\":\"_User\",\"objectId\":\"48lhphWWP6\"}}";
    public final static String HEADER_APP_ID = "X-Parse-Application-Id";
    public final static String HEADER_APP_ID_VALUE = "AAvgI84rQHpuA6NRVcyB6xz4zOYL3y94otjf98wf";
    public final static String HEADER_REST_KEY = "X-Parse-REST-API-Key";
    public final static String HEADER_REST_KEY_VALUE = "RsVCXdvtbRGv8Lxz9pGllBRUUIfgePa8bLNu1qrx";

    public static final String NAME = "Name";
    public static final String COUNTRY = "Country";
    public static final String POPULATION = "Population";
    public static final String CITY_ID = "CityId";
    public static final String USER = "User";
    public static final String USER_TYPE = "__type";
    public static final String USER_TYPE_VALUE = "Pointer";
    public static final String USER_CLASS_NAME = "className";
    public static final String USER_CLASS_NAME_VALUE = "_User";
    public static final String USER_OBJECT_ID = "objectId";
    public static final String USER_OBJECT_ID_VALUE = "48lhphWWP6";


    String result = null;

    public ArrayList<City> loadCities()
    {
        String response = null;
        java.net.URL url;
        HttpsURLConnection connection = null;

        try
        {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("where", ServerModel.WHERE_VALUE));
            url = new URL(ServerModel.URL + createParamsStringForHttpGetRequest(params));

            connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty(ServerModel.HEADER_APP_ID, ServerModel.HEADER_APP_ID_VALUE);
            connection.setRequestProperty(ServerModel.HEADER_REST_KEY, ServerModel.HEADER_REST_KEY_VALUE);
            connection.setRequestMethod("GET");
            connection.getDoInput();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);

            connection.connect();

            int responseCode = connection.getResponseCode();
            Log.d(TAG, "responseCode = " + responseCode);
            if (responseCode == 200)
            {
                response = ServerModel.readStream(connection.getInputStream());
            }
            else if (responseCode >= 400)
            {
                ServerModel.readStream(connection.getErrorStream());
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (connection != null)
                connection.disconnect();
        }

        return CityJSONParser.getCities(response);
    }

    public String addCity(String cityName, String countryName, int population)
    {
        String response = null;
        URL url;
        HttpsURLConnection connection = null;

        try
        {
            url = new URL(ServerModel.URL);

            connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty(ServerModel.HEADER_APP_ID, ServerModel.HEADER_APP_ID_VALUE);
            connection.setRequestProperty(ServerModel.HEADER_REST_KEY, ServerModel.HEADER_REST_KEY_VALUE);
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);


            JSONObject jsonParam = new JSONObject();
            jsonParam.put(ServerModel.CITY_ID, "");
            jsonParam.put(ServerModel.NAME, cityName);
            jsonParam.put(ServerModel.COUNTRY, countryName);
            jsonParam.put(ServerModel.POPULATION, population);

            JSONObject userJson = new JSONObject();
            userJson.put(ServerModel.USER_TYPE, ServerModel.USER_TYPE_VALUE);
            userJson.put(ServerModel.USER_CLASS_NAME, ServerModel.USER_CLASS_NAME_VALUE);
            userJson.put(ServerModel.USER_OBJECT_ID, ServerModel.USER_OBJECT_ID_VALUE);

            jsonParam.put(ServerModel.USER, userJson);

            Log.d(TAG, jsonParam.toString());

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(jsonParam.toString());
            writer.flush();
            writer.close();

            connection.connect();

            int responseCode = connection.getResponseCode();
            Log.d(TAG, "responseCode = " + responseCode);
            if (responseCode == 201)
            {
                response = ServerModel.readStream(connection.getInputStream());
            }
            else
            {
                ServerModel.readStream(connection.getErrorStream());
                return "FAILED";
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (connection != null)
                connection.disconnect();
        }

        return CityJSONParser.getAddedCityId(response);

    }

    public String deleteCity(String id)
    {
        String response = null;
        URL url;
        HttpsURLConnection connection = null;

        try
        {
            url = new URL(ServerModel.URL + "/" + id);

            connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty(ServerModel.HEADER_APP_ID, ServerModel.HEADER_APP_ID_VALUE);
            connection.setRequestProperty(ServerModel.HEADER_REST_KEY, ServerModel.HEADER_REST_KEY_VALUE);
            connection.setRequestMethod("DELETE");
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);

            connection.connect();

            int responseCode = connection.getResponseCode();
            Log.d(TAG, "responseCode = " + responseCode);
            if (responseCode == 200)
            {
                return "OK";
            }
            else
            {
                ServerModel.readStream(connection.getErrorStream());
                return "FAILED";
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (connection != null)
                connection.disconnect();
        }

        return "FAILED";

    }


    private static String readStream(InputStream inputStream)
    {
        BufferedReader reader = null;
        StringBuilder response = new StringBuilder();
        try
        {
            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while ((line = reader.readLine()) != null)
                response.append(line);
        }
        catch (IOException e)
        {

        }
        finally
        {
            if (reader != null)
                try
                {
                    reader.close();
                } catch (IOException e)
                {

                }

        }

        Log.d(TAG, "ResponseString =" + response.toString());

        return response.toString();
    }


    private String createParamsStringForHttpGetRequest(List<NameValuePair> nameValuePairParams) {
        if (nameValuePairParams == null || nameValuePairParams.size() == 0) return "";
        return "?" + URLEncodedUtils.format(nameValuePairParams, HTTP.UTF_8);
    }
}
