package android.kuva.com.server;

import com.android.kuva.mymodule.City;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by KuVa on 13.08.2015.
 */
public class CityJSONParser
{
    private static final String RESULT = "results";
    private static final String ID = "objectId";
    private static final String NAME = "Name";
    private static final String COUNTRY = "Country";
    private static final String POPULATION = "Population";

    public static ArrayList<City> getCities(String response)
    {
        ArrayList<City> cities = new ArrayList<>();

        try
        {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray(RESULT);

            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject object = jsonArray.getJSONObject(i);

                String id = object.getString(ID);
                String name = object.getString(NAME);
                String country = object.getString(COUNTRY);
                int population = object.getInt(POPULATION);

                cities.add(new City(id, name, country, population, "default.jpg"));
            }
        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        return cities;
    }

    public static String getAddedCityId(String response)
    {
        String id = null;

        try
        {
            JSONObject jsonObject = new JSONObject(response);
            id = jsonObject.getString(ID);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return id;
    }

}
