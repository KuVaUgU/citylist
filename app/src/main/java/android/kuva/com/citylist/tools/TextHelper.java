package android.kuva.com.citylist.tools;

/**
 * Created by MSI on 10.08.2015.
 */
public class TextHelper
{
    public static String formatPopulation(int population)
    {
        if (population == 0)
            return String.valueOf(0);

        if (population % 1000000 == 0)
            return String.valueOf(population / 1000000) + " млн.";
        else if (population % 1000 == 0 && population/1000000 > 0)
            return String.valueOf(population / 1000000) + " млн. " + String.valueOf((population % 1000000) / 1000) + " тыс.";
        else if (population % 1000 == 0)
            return String.valueOf(population/1000) + " тыс";

        return String.valueOf(population);
    }

}
