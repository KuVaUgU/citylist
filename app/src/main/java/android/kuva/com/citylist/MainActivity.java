package android.kuva.com.citylist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.kuva.com.citylist.tools.FontHelper;
import android.kuva.com.citylist.tools.TextHelper;
import android.kuva.com.megamodule.CityLab;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.android.kuva.mymodule.City;
import com.melnykov.fab.FloatingActionButton;


import java.util.ArrayList;


public class MainActivity extends AppCompatActivity
{
    private static final String CHECK_BOX_STATE_LIST = "CHECK_BOX_STATE_LIST";

    private ListView listView;
    private CityAdapter adapter;
    private TextView totalPopulationTextView;
    private CityLab cityLab;
    private FloatingActionButton addButton;
    private ProgressBar progressBar;

    private int totalPopulation;

    private ArrayList<City> cityList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cityLab = ((MegaApplication) getApplication()).getCityLab();
        FontHelper fontHelper = FontHelper.getFontsHelper(this);

        progressBar = (ProgressBar) findViewById(R.id.main_progressBar);

        listView = (ListView) findViewById(android.R.id.list);

        totalPopulationTextView = (TextView) findViewById(R.id.population_textView);
        totalPopulationTextView.setTypeface(fontHelper.getRobotoRegular());

        cityList = new ArrayList<>();
        adapter = new CityAdapter(this, cityList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                City city = cityList.get(position);
                CityActivity.startActivity(MainActivity.this, cityList.get(position).getId());
            }
        });
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener()
        {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked)
            {

            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu)
            {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.city_list_item_context, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu)
            {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item)
            {
               return onDeleteMenuButtonClicked(mode, item);
            }

            @Override
            public void onDestroyActionMode(ActionMode mode)
            {

            }
        });


        addButton = (FloatingActionButton) findViewById(R.id.fab);
        addButton.attachToListView(listView);
        addButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                AddCityActivity.startActivity(MainActivity.this);
            }
        });

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        new LoadCitiesTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        switch (id)
        {
            case R.id.menu_add_city:
                AddCityActivity.startActivity(MainActivity.this);
                break;
            case R.id.menu_graph:
                ArrayList<String> checkedIds = getCheckedIds();

                if (checkedIds.size() > 0)
                {
                    GraphActivity.startActivity(MainActivity.this, totalPopulation, getCheckedIds());
                } else
                {
                    Toast.makeText(MainActivity.this, "Check something MAN", Toast.LENGTH_SHORT).show();
                }
                break;
        }

        return true;
    }

    public void updateCount()
    {
        ArrayList<Boolean> checkBoxStateList = adapter.getCheckBoxStateList();
        int count = 0;
        for (int i = 0; i < checkBoxStateList.size(); i++)
            if (checkBoxStateList.get(i))
                count += (adapter.getItem(i)).getPopulation();

        totalPopulation = count;
        totalPopulationTextView.setText(getString(R.string.total_population) + " " + TextHelper.formatPopulation(count));
    }


    public ArrayList<String> getCheckedIds()
    {
        ArrayList<String> checkedIds = new ArrayList<>();
        ArrayList<Boolean> checkedBoxStateList = adapter.getCheckBoxStateList();

        for (int i = 0; i < checkedBoxStateList.size(); i++)
            if (checkedBoxStateList.get(i))
                checkedIds.add( (adapter.getItem(i)).getId() );

        return checkedIds;
    }

    private void progressBarShow(boolean bool)
    {
        if (bool)
        {
            progressBar.setVisibility(View.VISIBLE);
            addButton.setVisibility(View.GONE);
            listView.setVisibility(View.GONE);
            totalPopulationTextView.setVisibility(View.INVISIBLE);
        }
        else
        {
            progressBar.setVisibility(View.GONE);
            addButton.setVisibility(View.VISIBLE);
            listView.setVisibility(View.VISIBLE);
            totalPopulationTextView.setVisibility(View.VISIBLE);
        }
    }

    private boolean onDeleteMenuButtonClicked(final ActionMode mode, MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_item_delete_city:
                AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                dialog.setTitle("Вы уверены?");
                dialog.setPositiveButton("Да", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        ArrayList<Boolean> checkBoxStateList = adapter.getCheckBoxStateList();
                        ArrayList<String> ids = new ArrayList<>();
                        for (int i = adapter.getCount() - 1; i >= 0; i--)
                            if (listView.isItemChecked(i))
                            {
                                ids.add((adapter.getItem(i)).getId());
                                checkBoxStateList.remove(i);
                                cityList.remove(i);
                            }
                        new DeleteCitiesTask().execute(ids);       // delete process
                        mode.finish();
                    }
                });
                dialog.setNegativeButton("Нет", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        mode.finish();
                    }
                });
                dialog.setCancelable(true);
                dialog.setOnCancelListener(null);
                dialog.show();
                return true;
            default:
                return false;
        }
    }


    private class LoadCitiesTask extends AsyncTask<Void, Void, ArrayList<City>>
    {
        public final static String TAG = "ServerModel";


        @Override
        protected void onPreExecute() {
            progressBarShow(true);
        }

        @Override
        protected ArrayList<City> doInBackground(Void... params)
        {
            return cityLab.loadCities();

        }

        @Override
        protected void onPostExecute(ArrayList<City> cities)
        {
            cityList.clear();
            for (int i = 0; i < cities.size(); i++)
                cityList.add(cities.get(i));

            adapter.notifyDataSetChanged();
            updateCount();
            progressBarShow(false);
        }
    }

    private class DeleteCitiesTask extends AsyncTask<ArrayList<String> , Void, Void>
    {

        @Override
        protected Void doInBackground(ArrayList<String>... params)
        {
            ArrayList<String> ids = params[0];

            for (int i = 0; i < ids.size(); i++)
                cityLab.deleteCity(ids.get(i));

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            adapter.notifyDataSetChanged();
            updateCount();
        }
    }
/*
    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        ArrayList<Boolean> checkBoxStateList = adapter.getCheckBoxStateList();
        boolean [] mas = new boolean[checkBoxStateList.size()];

        for (int i = 0; i < checkBoxStateList.size(); i++)
        {
            mas[i] = checkBoxStateList.get(i);
        }

        outState.putBooleanArray(CHECK_BOX_STATE_LIST, mas);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        ArrayList<Boolean> checkBoxStateList = new ArrayList<>();
        boolean [] mas = savedInstanceState.getBooleanArray(CHECK_BOX_STATE_LIST);

        for (int i = 0; i < mas.length; i++)
            checkBoxStateList.add(mas[i]);

        adapter.setCheckBoxStateList(checkBoxStateList);
        super.onRestoreInstanceState(savedInstanceState);
    } */
}
