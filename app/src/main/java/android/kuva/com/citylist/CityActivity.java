package android.kuva.com.citylist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Matrix;

import android.graphics.drawable.Drawable;
import android.kuva.com.citylist.tools.FontHelper;
import android.kuva.com.megamodule.CityLab;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.kuva.mymodule.City;


import java.io.IOException;
import java.io.InputStream;

/**
 * Created by KuVa on 02.08.2015.
 */
public class CityActivity extends Activity
{
    public final static String CITY_ID = "CITY_ID";

    private ImageView imageView;
    private Matrix matrix;
    private SeekBar seekBar;
    private EditText populationEditText;
    private Button okButton;
    private TextView textView;
    private CityLab cityLab;
    private City city;

    public static void startActivity(Context context, String id)
    {
        Intent intent = new Intent(context, CityActivity.class);
        intent.putExtra(CITY_ID, id);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        cityLab = ((MegaApplication)getApplication()).getCityLab();
        FontHelper fontHelper = FontHelper.getFontsHelper(this);

        city = cityLab.getCity(getIntent().getStringExtra(CITY_ID));

        AssetManager manager = getAssets();

        populationEditText = (EditText) findViewById(R.id.city_edit_text);
        populationEditText.setText(String.valueOf(city.getPopulation()));
        populationEditText.setTypeface(fontHelper.getRobotoRegular());

        textView = (TextView) findViewById(R.id.city_textView);
        textView.setTypeface(fontHelper.getRobotoRegular());

        okButton = (Button) findViewById(R.id.ok_button);
        okButton.setTypeface(fontHelper.getRobotoRegular());

        okButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                saveNewPopulation();
                finish();
            }
        });

        imageView = (ImageView) findViewById(R.id.city_image_view);

        try
        {
            InputStream inputStream = manager.open(city.getImgSrc());
            Drawable d = Drawable.createFromStream(inputStream, null);
            imageView.setImageDrawable(d);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        matrix = new Matrix();

        seekBar = (SeekBar) findViewById(R.id.zoom_seek_bar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
            {
                if (progress!= 0)
                {
                    matrix.setScale((float)progress/3, (float) progress/3);
                    imageView.setImageMatrix(matrix);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {

            }
        });

    }

    private void saveNewPopulation ()
    {
        int newPopulation = Integer.parseInt(populationEditText.getText().toString());

        if (newPopulation != city.getPopulation())
        {

            cityLab.saveNewPopulation(city.getId(), newPopulation);
        }
    }
}
