package android.kuva.com.citylist.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.kuva.com.citylist.tools.TextHelper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.android.kuva.mymodule.City;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by MSI on 07.08.2015.
 */
public class GraphView extends View
{
    private ArrayList<City> cities;
    private ArrayList<RGB> rgbs;

    private float offset = 0;
    private int dx;

    private int cityLineWidth = 50;
    private int coordinateLineWidth = 8;
    private int populationTextSize = 20;
    private int cityNameTextSize = 26;

    private int padForArrow = 15;   // important shit
    private int botPadding = 200;
    private int populationTextPadding = 50;
    private int stepX = 100;
    private int leftSpace = 50;
    private int rightSpace = 50;

    private int widthNeeded;
    private int maxPopulation;

    private Paint populationPaintText;
    private Paint cityNamePaintText;
    private Paint paintLine;
    private Paint paintCityLine;


    public GraphView(Context context)
    {
        super(context);
        initialize();
    }

    public GraphView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initialize();
    }

    public GraphView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    private void initialize()
    {
        paintLine = new Paint();
        paintLine.setColor(Color.BLACK);
        paintLine.setStrokeWidth(coordinateLineWidth);

        paintCityLine = new Paint();
        paintCityLine.setStrokeWidth(cityLineWidth);

        populationPaintText = new Paint();
        populationPaintText.setColor(Color.BLACK);
        populationPaintText.setTextSize(populationTextSize);
        populationPaintText.setTextAlign(Paint.Align.CENTER);
        populationPaintText.setAntiAlias(true);

        cityNamePaintText = new Paint();
        cityNamePaintText.setColor(Color.BLACK);
        cityNamePaintText.setTextSize(cityNameTextSize);
        cityNamePaintText.setTextAlign(Paint.Align.CENTER);
        cityNamePaintText.setFakeBoldText(true);
        cityNamePaintText.setAntiAlias(true);
    }


    @Override
    protected void onDraw(Canvas canvas)
    {
        if (canvas.getWidth() - offset - rightSpace >= widthNeeded)
        {
           offset = canvas.getWidth() - widthNeeded - rightSpace;
        }
            if (widthNeeded > canvas.getWidth())    // offset = 0
            {
                canvas.save();
                canvas.translate(offset, 0);
            }

            drawCitiesLines(canvas);
            drawCoordinates(canvas);
            drawRotatedText(canvas);

            if (widthNeeded > canvas.getWidth())
                canvas.restore();


        super.onDraw(canvas);
    }

    private void drawCoordinates(Canvas canvas)
    {
        int height = canvas.getHeight();

        canvas.drawLine(padForArrow + leftSpace, height - padForArrow - botPadding, padForArrow + leftSpace, 0, paintLine);  // y
        canvas.drawLine(padForArrow + leftSpace, 0, leftSpace, padForArrow * 2, paintLine);                            //arrows
        canvas.drawLine(padForArrow + leftSpace, 0, padForArrow * 2 + leftSpace, padForArrow * 2, paintLine);

        canvas.drawLine(padForArrow + leftSpace, height - padForArrow - botPadding, widthNeeded + 2, height - padForArrow - botPadding, paintLine);  // x
        canvas.drawLine(widthNeeded, height - padForArrow - botPadding, widthNeeded - 2 * padForArrow, height - botPadding, paintLine);              //arrows
        canvas.drawLine(widthNeeded, height - padForArrow - botPadding, widthNeeded - 2 * padForArrow, height - 2 * padForArrow - botPadding, paintLine);

        canvas.drawPoint(padForArrow + leftSpace, height - padForArrow - botPadding, paintLine);    // dot

    }

    private void drawCitiesLines(Canvas canvas)
    {
        int height = canvas.getHeight();

        float scale = (height - padForArrow - botPadding - populationTextPadding) / (float) maxPopulation;
        int x = stepX + padForArrow + leftSpace;

        for (int i = 0; i < cities.size(); i++)
        {
            RGB rgb = rgbs.get(i);
            paintCityLine.setARGB(255, rgb.getR(), rgb.getG(), rgb.getB());
            float lineHeight = height - padForArrow - botPadding - (cities.get(i).getPopulation()*scale);
            canvas.drawLine(x, height - botPadding - padForArrow, x , lineHeight, paintCityLine);
            canvas.drawText(TextHelper.formatPopulation(cities.get(i).getPopulation()), x, lineHeight - (populationTextPadding/2) , populationPaintText);
            x += stepX;
        }

    }

    public void drawRotatedText(Canvas canvas)
    {
        int x = stepX + padForArrow + leftSpace;
        int height = canvas.getHeight();
        int y = height - (botPadding /2) + 10;
        canvas.save();
        canvas.rotate(-90, x, height - (botPadding / 2));

        for (City city : cities)
        {
            canvas.drawText(city.getName(), x, y, cityNamePaintText);
            y += stepX;
        }
        canvas.restore();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                dx = (int) event.getRawX() - (int) offset;
                break;
            case MotionEvent.ACTION_MOVE:
                float moveX = event.getRawX() - dx;
                if (moveX < 10)
                {
                    offset = moveX;
                    invalidate();
                }
                Log.d("MoveCanvas", String.valueOf(moveX) + " " + String.valueOf(widthNeeded));
                break;
        }
        return true;
    }

    public void setCities(ArrayList<City> cities)
    {
        this.cities = cities;
        widthNeeded = (cities.size() * stepX) + padForArrow + rightSpace + 2*leftSpace;

        Random rnd = new Random();
        rgbs = new ArrayList<>();
        maxPopulation = 0;
        for (int i = 0; i < cities.size(); i++)
        {
            rgbs.add(new RGB(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));

            if (maxPopulation < cities.get(i).getPopulation())
                maxPopulation = cities.get(i).getPopulation();
        }
    }
}
