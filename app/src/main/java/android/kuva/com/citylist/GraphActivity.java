package android.kuva.com.citylist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.kuva.com.citylist.tools.FontHelper;
import android.kuva.com.citylist.tools.TextHelper;
import android.kuva.com.citylist.views.GraphView;
import android.kuva.com.megamodule.CityLab;
import android.os.Bundle;
import android.widget.TextView;


import java.util.ArrayList;

/**
 * Created by MSI on 07.08.2015.
 */
public class GraphActivity extends Activity
{
    public static final String TOTAL_POPULATION = "TOTAL_POPULATION";
    public static final String CHECKED_IDS = "CHECKED_IDS";

    private TextView totalPopulationTextView;
    private CityLab cityLab;
    private GraphView graphView;

    public static void startActivity(Context context, int totalPopulation, ArrayList<String> checkedIds)
    {
        Intent intent = new Intent(context, GraphActivity.class);
        intent.putExtra(TOTAL_POPULATION, totalPopulation);
        intent.putExtra(CHECKED_IDS, checkedIds);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        int totalPopulation = getIntent().getIntExtra(TOTAL_POPULATION, 0);
        ArrayList<String> checkedIds = getIntent().getStringArrayListExtra(CHECKED_IDS);

        FontHelper fontHelper = FontHelper.getFontsHelper(this);

        totalPopulationTextView = (TextView) findViewById(R.id.population_graph_textView);
        totalPopulationTextView.setText(getString(R.string.total_population) + " " + TextHelper.formatPopulation(totalPopulation));
        totalPopulationTextView.setTypeface(fontHelper.getRobotoRegular());

        cityLab = ((MegaApplication) getApplication()).getCityLab();

        graphView = (GraphView) findViewById(R.id.graph_view);
         graphView.setCities(cityLab.getCities(checkedIds));

    }

}
