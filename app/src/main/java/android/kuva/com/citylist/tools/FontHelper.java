package android.kuva.com.citylist.tools;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;

/**
 * Created by KuVa on 10.08.2015.
 */
public class FontHelper
{
    private AssetManager manager;

    private Typeface robotoBold;
    private Typeface robotoRegular;

    private static FontHelper fontsHelper;

    private FontHelper(Context context)
    {
        manager = context.getAssets();
        robotoBold = Typeface.createFromAsset(manager, "fonts/Roboto-LightItalic.ttf");
        robotoRegular = Typeface.createFromAsset(manager, "fonts/Roboto-Regular.ttf");
    }

    public static FontHelper getFontsHelper(Context context)
    {

        if (fontsHelper == null)
            fontsHelper = new FontHelper(context);

        return fontsHelper;
    }

    public Typeface getRobotoBold()
    {
        return robotoBold;
    }

    public Typeface getRobotoRegular()
    {
        return robotoRegular;
    }
}
