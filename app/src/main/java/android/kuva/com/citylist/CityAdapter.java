package android.kuva.com.citylist;

import android.kuva.com.citylist.tools.FontHelper;
import android.kuva.com.citylist.tools.TextHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.android.kuva.mymodule.City;

import java.util.ArrayList;

/**
 * Created by MSI on 05.08.2015.
 */
public class CityAdapter extends ArrayAdapter<City>
{
    private ArrayList<Boolean> checkBoxStateList;
    private FontHelper fontHelper;
    private MainActivity activity;

    public CityAdapter(MainActivity activity, ArrayList<City> cityList)
    {
        super(activity, 0, cityList);
        this.activity = activity;
        fontHelper = FontHelper.getFontsHelper(activity);
        checkBoxStateList = new ArrayList<>();
        for (int i = 0; i < cityList.size(); i++)
            checkBoxStateList.add(false);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        City city = getItem(position);
        //ViewHolder viewHolder;

        if (convertView == null)
        {
            convertView = activity.getLayoutInflater().inflate(R.layout.list_item_city, null);
            CheckBox countCheckBox = (CheckBox) convertView.findViewById(R.id.city_list_item_countCheckBox);
            countCheckBox.setTag(position);

            countCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                {
                    int pos = (int) buttonView.getTag();
                    if (isChecked != checkBoxStateList.get(pos))
                    {
                        checkBoxStateList.set(pos, isChecked);
                        activity.updateCount();
                    }
                }
            });

            TextView nameTextView = (TextView) convertView.findViewById(R.id.city_list_item_nameTextView);
            nameTextView.setTypeface(fontHelper.getRobotoBold());
            nameTextView.setText(city.getName());

            TextView countyTextView = (TextView) convertView.findViewById(R.id.city_list_item_countyTextView);
            countyTextView.setText(city.getCountry());
            countyTextView.setTypeface(fontHelper.getRobotoRegular());

            TextView populationTextView = (TextView) convertView.findViewById(R.id.city_list_item_populationTextView);
            populationTextView.setText(activity.getString(R.string.population) + " " + TextHelper.formatPopulation(city.getPopulation()));
            populationTextView.setTypeface(fontHelper.getRobotoRegular());

        }
        else
        {

            //ViewHolder
            TextView nameTextView = (TextView) convertView.findViewById(R.id.city_list_item_nameTextView);
            nameTextView.setText(city.getName());

            TextView countyTextView = (TextView) convertView.findViewById(R.id.city_list_item_countyTextView);
            countyTextView.setText(city.getCountry());

            TextView populationTextView = (TextView) convertView.findViewById(R.id.city_list_item_populationTextView);
            populationTextView.setText(activity.getString(R.string.population) + " " + TextHelper.formatPopulation(city.getPopulation()));

            CheckBox countCheckBox = (CheckBox) convertView.findViewById(R.id.city_list_item_countCheckBox);
            countCheckBox.setTag(position);
            countCheckBox.setChecked(checkBoxStateList.get(position));
        }

        return convertView;
    }

    public void setCheckBoxStateList(ArrayList<Boolean> checkBoxStateList)
    {
        this.checkBoxStateList = checkBoxStateList;
    }

    public ArrayList<Boolean> getCheckBoxStateList() {
        return checkBoxStateList;
    }

    @Override
    public void notifyDataSetChanged()
    {
        super.notifyDataSetChanged();
        checkBoxStateList = new ArrayList<>();
        for (int i = 0; i < getCount(); i++)
            checkBoxStateList.add(false);

    }

    /*  private ArrayList<City> cityList;
    private MainActivity activity;
    private FontHelper fontHelper;

    private ArrayList<Boolean> checkBoxStateList;   // in bundle

    public CityAdapter (ArrayList<City> cityList, Activity activity)
    {
        this.cityList = cityList;
        this.activity = (MainActivity) activity;
        fontHelper = FontHelper.getFontsHelper(activity);

        checkBoxStateList = new ArrayList<>();
        for (int i = 0; i < cityList.size(); i++)
            checkBoxStateList.add(false);
    }

    @Override
    public int getCount()
    {
        return cityList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return cityList.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        City city = cityList.get(position);
        //ViewHolder viewHolder;

        if (convertView == null)
        {
            convertView = activity.getLayoutInflater().inflate(R.layout.list_item_city, null);
            CheckBox countCheckBox = (CheckBox) convertView.findViewById(R.id.city_list_item_countCheckBox);
            countCheckBox.setTag(position);

            countCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                {
                    int pos = (int) buttonView.getTag();
                    if (isChecked != checkBoxStateList.get(pos))
                    {
                        checkBoxStateList.set(pos, isChecked);
                        activity.updateCount();
                    }
                }
            });

            TextView nameTextView = (TextView) convertView.findViewById(R.id.city_list_item_nameTextView);
            nameTextView.setTypeface(fontHelper.getLCDNovaTypeface());
            nameTextView.setText(city.getName());

            TextView countyTextView = (TextView) convertView.findViewById(R.id.city_list_item_countyTextView);
            countyTextView.setText(city.getCountry());
            countyTextView.setTypeface(fontHelper.getRobotoBold());

            TextView populationTextView = (TextView) convertView.findViewById(R.id.city_list_item_populationTextView);
            populationTextView.setText(activity.getString(R.string.population) + " " + TextHelper.formatPopulation(city.getPopulation()));
            populationTextView.setTypeface(fontHelper.getRobotoRegular());

        }
        else
        {

                                                                                                            //ViewHolder
            TextView nameTextView = (TextView) convertView.findViewById(R.id.city_list_item_nameTextView);
            nameTextView.setText(city.getName());

            TextView countyTextView = (TextView) convertView.findViewById(R.id.city_list_item_countyTextView);
            countyTextView.setText(city.getCountry());

            TextView populationTextView = (TextView) convertView.findViewById(R.id.city_list_item_populationTextView);
            populationTextView.setText(activity.getString(R.string.population) + " " + TextHelper.formatPopulation(city.getPopulation()));

            CheckBox countCheckBox = (CheckBox) convertView.findViewById(R.id.city_list_item_countCheckBox);
            countCheckBox.setTag(position);
            countCheckBox.setChecked(checkBoxStateList.get(position));
        }

        return convertView;
    }

    public void setCityList(ArrayList <City> cityList)
    {
        this.cityList = cityList;

        checkBoxStateList = new ArrayList<>();
        for (int i = 0; i < cityList.size(); i++)
            checkBoxStateList.add(false);

    }

    public void setCheckBoxStateList(ArrayList<Boolean> checkBoxStateList)
    {
        this.checkBoxStateList = checkBoxStateList;
    }

    public ArrayList<Boolean> getCheckBoxStateList() {
        return checkBoxStateList;
    }
    */
}