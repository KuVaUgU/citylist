package android.kuva.com.citylist;

import android.app.Application;
import android.kuva.com.megamodule.CityLab;


/**
 * Created by MSI on 05.08.2015.
 */
public class MegaApplication extends Application
{

    private CityLab cityLab;

    @Override
    public void onCreate()
    {
        super.onCreate();
        cityLab = new CityLab(this);
    }

    public CityLab getCityLab()
    {
        return cityLab;
    }
}
