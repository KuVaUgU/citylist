package android.kuva.com.citylist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.kuva.com.megamodule.CityLab;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;


/**
 * Created by KuVa on 10.08.2015.
 */
public class AddCityActivity extends Activity
{

    private EditText cityNameEditText;
    private EditText countryNameEditText;
    private EditText populationEditText;
    private Button addPhotoButton;
    private Button okButton;
    private ProgressBar progressBar;

    private CityLab cityLab;

    public static void startActivity(Context context)
    {
        Intent intent = new Intent(context, AddCityActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_city);

        cityLab = ((MegaApplication) getApplication()).getCityLab();

        progressBar = (ProgressBar) findViewById(R.id.add_progressBar);
        cityNameEditText = (EditText) findViewById(R.id.city_name_editText);
        countryNameEditText = (EditText) findViewById(R.id.country_name_editText);
        populationEditText = (EditText) findViewById(R.id.population_editText);

        addPhotoButton = (Button) findViewById(R.id.add_photo_button);
        addPhotoButton.setEnabled(false);

        okButton = (Button) findViewById(R.id.add_ok_button);
        okButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isFieldsOk())
                {
                    new AddCityTask().execute();
                    showUI(false);
                } else
                {
                    Toast.makeText(AddCityActivity.this, "Заполните поля", Toast.LENGTH_SHORT).show();
                }
            }
        });

        showUI(true);

    }


    private boolean isFieldsOk()
    {
        if (cityNameEditText.getText().toString().isEmpty() ||
                countryNameEditText.getText().toString().isEmpty() ||
                populationEditText.getText().toString().isEmpty())
            return false;

        return true;
    }

    private void showUI(boolean bool)
    {
        if (bool)
        {
            okButton.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
        else
        {
            okButton.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    private class AddCityTask extends AsyncTask <Void, Void, Void>
    {
        @Override
        protected Void doInBackground(Void... params)
        {
            cityLab.addCity(cityNameEditText.getText().toString(),
                    countryNameEditText.getText().toString(),
                    Integer.parseInt(populationEditText.getText().toString()),
                    "default.jpg");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            finish();
        }
    }

}
