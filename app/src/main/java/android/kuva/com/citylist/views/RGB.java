package android.kuva.com.citylist.views;

/**
 * Created by KuVa on 10.08.2015.
 */
public class RGB
{
    private int r;
    private int g;
    private int b;

    public RGB(int r, int g, int b)
    {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public int getR()
    {
        return r;
    }

    public int getG()
    {
        return g;
    }

    public int getB()
    {
        return b;
    }
}
