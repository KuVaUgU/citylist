package android.kuva.com.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by KuVa on 10.08.2015.
 */
 class DBHelper extends SQLiteOpenHelper
{

    private final static String TAG = "MyDataBase";

    private final static String CITIES_TABLE = "cities";

    public DBHelper(Context context, String name, int version)
    {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        Log.d(TAG, "--- onCreate database ---");

        db.execSQL("create table " + CITIES_TABLE +
                " (" + Columns.ID_COLUMN + " varchar(60) PRIMARY KEY, " +
                Columns.CITY_NAME_COLUMN + " varchar(60), " +
                Columns.COUNTY_NAME_COLUMN + " varchar(60), " +
                Columns.POPULATION_COLUMN + " integer, " +
                Columns.IMG_SRC_COLUMN + " varchar(60), " +
                Columns.STATE + " varchar(60));");

        Log.d(TAG, "--- Database created ---");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        //empty
    }
}
