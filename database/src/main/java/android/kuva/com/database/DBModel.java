package android.kuva.com.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.android.kuva.mymodule.City;

import java.util.ArrayList;

/**
 * Created by KuVa on 02.08.2015.
 */
public class DBModel
{
    private final static String TAG = "MyDataBase";

    private static final String DB_NAME = "cityListDataBase";
    private static final String CITIES_TABLE = "cities";

    private DBHelper dbHelper;
    private Context appContext;

    public DBModel(Context appContext)
    {
        this.appContext = appContext;
    }

    private void initializeDataBase()
    {
        dbHelper = new DBHelper(appContext,DB_NAME, 1);
    }

    public ArrayList<City> loadCities()
    {
        if (dbHelper == null)
            initializeDataBase();

        ArrayList<City> cities = new ArrayList<>();

        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM " + CITIES_TABLE, null);

        if (cursor != null)
        {
            if (cursor.moveToFirst())
            {
                do
                {
                    String id = cursor.getString(cursor.getColumnIndex(Columns.ID_COLUMN));
                    String name = cursor.getString(cursor.getColumnIndex(Columns.CITY_NAME_COLUMN));
                    String country = cursor.getString(cursor.getColumnIndex(Columns.COUNTY_NAME_COLUMN));
                    int population = cursor.getInt(cursor.getColumnIndex(Columns.POPULATION_COLUMN));
                    String imgSrc = cursor.getString(cursor.getColumnIndex(Columns.IMG_SRC_COLUMN));

                    cities.add(new City(id, name, country, population, imgSrc));
                } while (cursor.moveToNext());

            } else
                Log.d(TAG, "Cursor is empty");

            cursor.close();

            dbHelper.close();
        }

        return cities;
    }

    public void addCity(String id, String cityName, String countryName, int population, String imgSrc)
    {
        if (dbHelper == null)
            initializeDataBase();

        SQLiteDatabase database = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Columns.ID_COLUMN, id);
        values.put(Columns.CITY_NAME_COLUMN, cityName);
        values.put(Columns.COUNTY_NAME_COLUMN, countryName);
        values.put(Columns.POPULATION_COLUMN, population);
        values.put(Columns.IMG_SRC_COLUMN, imgSrc);

        database.insertWithOnConflict(CITIES_TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        Log.d(TAG, cityName + " Inserted");

        dbHelper.close();
    }

    public City getCity(String id)
    {
        if (dbHelper == null)
            initializeDataBase();

        SQLiteDatabase database = dbHelper.getReadableDatabase();

        Cursor cursor = database.rawQuery("SELECT * from " + CITIES_TABLE +
                " WHERE " + Columns.ID_COLUMN + "='" + id  + "'" ,null);

        if (cursor.moveToFirst())
        {
            String name = cursor.getString(cursor.getColumnIndex(Columns.CITY_NAME_COLUMN));
            String country = cursor.getString(cursor.getColumnIndex(Columns.COUNTY_NAME_COLUMN));
            int population = cursor.getInt(cursor.getColumnIndex(Columns.POPULATION_COLUMN));
            String imgSrc = cursor.getString(cursor.getColumnIndex(Columns.IMG_SRC_COLUMN));

            cursor.close();
            dbHelper.close();
            return new City(id, name, country, population,imgSrc);
        }

        cursor.close();
        dbHelper.close();

        return null;
    }

    public ArrayList<City> getCities(ArrayList<String> ids)
    {
        if (dbHelper == null)
            initializeDataBase();

        ArrayList<City> cities = new ArrayList<>();

        StringBuilder builder = new StringBuilder("('" + ids.get(0) +"'");     // String for a query
        for (int i = 1; i < ids.size(); i++)
        {
            builder.append(", '" + ids.get(i)+"'");
        }
        builder.append(")");

        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM " + CITIES_TABLE +
                          " WHERE " + Columns.ID_COLUMN + " IN " + builder.toString(), null);

        if (cursor.moveToFirst())
        {
            do
            {
                String id = cursor.getString(cursor.getColumnIndex(Columns.ID_COLUMN));
                String name = cursor.getString(cursor.getColumnIndex(Columns.CITY_NAME_COLUMN));
                String country = cursor.getString(cursor.getColumnIndex(Columns.COUNTY_NAME_COLUMN));
                int population = cursor.getInt(cursor.getColumnIndex(Columns.POPULATION_COLUMN));
                String imgSrc = cursor.getString(cursor.getColumnIndex(Columns.IMG_SRC_COLUMN));

                cities.add(new City(id, name, country, population, imgSrc));
            } while (cursor.moveToNext());
        } else
        {
            Log.d(TAG, "Cursor is empty --- getCities()");
        }

        cursor.close();
        database.close();

        return cities;
    }

    public void saveNewPopulation(String id, int newPopulation)
    {
        if (dbHelper == null)
            initializeDataBase();

        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.execSQL("UPDATE " + CITIES_TABLE +
                " SET " + Columns.POPULATION_COLUMN + "=" + newPopulation +
                " WHERE " + Columns.ID_COLUMN + "='" + id + "'");

        dbHelper.close();
    }

    public void deleteCity(String id)
    {
        if (dbHelper == null)
            initializeDataBase();

        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.execSQL("DELETE FROM " + CITIES_TABLE +
                         " WHERE " + Columns.ID_COLUMN + "='" + id + "'");

        dbHelper.close();
    }

}
