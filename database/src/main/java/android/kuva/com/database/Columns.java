package android.kuva.com.database;

/**
 * Created by KuVa on 11.08.2015.
 */
 class Columns
{
    public static final String ID_COLUMN = "objectId";
    public static final String CITY_NAME_COLUMN = "name";
    public static final String COUNTY_NAME_COLUMN = "county";
    public static final String POPULATION_COLUMN= "population";
    public static final String IMG_SRC_COLUMN = "imgSrc";
    public static final String STATE = "state";
}
