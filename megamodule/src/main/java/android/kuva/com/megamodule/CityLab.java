package android.kuva.com.megamodule;


import android.content.Context;
import android.kuva.com.database.DBModel;
import android.kuva.com.server.ServerModel;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.android.kuva.mymodule.City;
import java.util.ArrayList;


/**
 * Created by KuVa on 02.08.2015.
 */
public class CityLab
{
    private Context appContext;
    private ServerModel serverModel;
    private DBModel dbModel;

    public CityLab(Context appContext)
    {
        this.appContext = appContext;
        serverModel = new ServerModel();
        dbModel = new DBModel(appContext);
    }


    public ArrayList<City> loadCities()
    {
        ArrayList<City> citiesFromServer = serverModel.loadCities();
        for (City city : citiesFromServer)
        {
            addCity(city);
        }

        return dbModel.loadCities();
    }

    public City getCity(String id)
    {
        return dbModel.getCity(id);
    }

    public void saveNewPopulation(String id, int newPopulation)
    {
        long time1 = System.currentTimeMillis();
        dbModel.saveNewPopulation(id, newPopulation);
        Log.d("TIME", String.valueOf(System.currentTimeMillis() - time1));
    }

    public void addCity(String cityName, String countryName, int population, String imgSrc)
    {

        String idResponse = serverModel.addCity(cityName, countryName, population);
        if (!idResponse.equals("FAILED"))
        {

            dbModel.addCity(idResponse, cityName, countryName, population, imgSrc);
        }
    }

    private void addCity(City city)
    {
        dbModel.addCity(city.getId(), city.getName(), city.getCountry(), city.getPopulation(), city.getImgSrc());
    }

    public void deleteCity(String id)
    {
        String response = serverModel.deleteCity(id);

        if (response.equals("OK"))
           dbModel.deleteCity(id);

    }

    public ArrayList<City> getCities(ArrayList<String> ids)
    {
        return dbModel.getCities(ids);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
