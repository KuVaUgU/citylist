package com.android.kuva.mymodule;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by KuVa on 02.08.2015.
 */
public class City implements Serializable
{
    static final long serialVersionUID = 4091843754909138711L;

    private String name;
    private String country;
    private int population;
    private String id;
    private String imgSrc;

    public City(String id, String name, String country, int population, String imgSrc)
    {
        this.name = name;
        this.country = country;
        this.population = population;
        this.id = id;
        this.imgSrc = imgSrc;
    }

    public String getName()
    {
        return name;
    }

    public String getCountry()
    {
        return country;
    }

    public int getPopulation()
    {
        return population;
    }

    public void setPopulation(int population)
    {
        this.population = population;
    }

    public String getId() {
        return id;
    }

    public String getImgSrc()
    {
        return imgSrc;
    }
}
